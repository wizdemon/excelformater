package com.ExcelArranger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcelArrangerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExcelArrangerApplication.class, args);
	}

}

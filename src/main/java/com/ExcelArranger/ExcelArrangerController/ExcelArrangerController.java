package com.ExcelArranger.ExcelArrangerController;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

//import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;


@Controller
public class ExcelArrangerController {
	
	{

	try {

        // list required column headers in order
        String[] outColumns = { "accountNo", "orderReference (Optional)",	"Status",	"Customer Name",	"Phone no",	
        		"Address",	"Feeder Line",	"Total Ampage",	"Separation Required",	"No of Service Wires",	"Condition of Wiring",	"Output Cable Distance (Load Wire)",	
        		"Input Cable Distance (Supply to Meter)",	"Cable size 16mm or 25mm or 35mm",	"Service Wire Traceable",	"Meter Point Wire Distribution",	"Front View (Picture)",	
        		"Meter Installation Point (Picture)",	"Meter Bill (Picture)",	"Meter Readiness",	"Recommendation",	"Meter Required",	"Installation Type",	"Customer Type", "Tariff Recommended",	"Customer Confirmation",	"Customer Signature",	"Engineer email"};
       
        FileInputStream excelFile = new FileInputStream(new File( "C:\\Users\\hp\\Documents\\FiletoUse\\SURVEYtemp.xlsx"));

        XSSFWorkbook workbook1 = new XSSFWorkbook(excelFile);
        XSSFSheet mainSheet = workbook1.getSheetAt(0);
       XSSFWorkbook outWorkBook = reArrange(mainSheet, mapHeaders(outColumns, mainSheet));
        excelFile.close();
        
        File mergedFile = new File("C:\\Users\\hp\\Documents\\NewFile\\OutputExcel.xlsx");
        if (!mergedFile.exists()) {
        mergedFile.createNewFile();
        }
        FileOutputStream out = new FileOutputStream(mergedFile);
        outWorkBook.write(out);
        out.close();
        System.out.println("File Columns Were Re-Arranged Successfully");
         } catch (Exception e)
	{
            e.printStackTrace();
    }

}
	public static XSSFWorkbook reArrange(XSSFSheet mainSheet, LinkedHashMap<String, Integer> map) {

        
        Set<String> colNumbs = map.keySet();

        XSSFWorkbook outWorkbook = new XSSFWorkbook();
        XSSFSheet outSheet = outWorkbook.createSheet();
        
       Map<Integer, XSSFCellStyle> styleMap = new HashMap<Integer, XSSFCellStyle>();
        
        int colNum = 0;
        XSSFRow hrow = outSheet.createRow(0);
        for (String col : colNumbs) {
            XSSFCell cell = hrow.createCell(colNum);
            cell.setCellValue(col);
            colNum++;
        }

       
        for (int j = mainSheet.getFirstRowNum() + 1; j <= mainSheet.getLastRowNum(); j++) {

            XSSFRow row = mainSheet.getRow(j);

            XSSFRow mrow = outSheet.createRow(j);
            int num = -1;
            for (String k : colNumbs) {
                Integer cellNum = map.get(k);
                num++;
                if (cellNum != null) {
                    XSSFCell cell = row.getCell(cellNum.intValue());
                      if(cell == null) {
                        continue;
                    }
                    XSSFCell mcell = mrow.createCell(num);

                    if (cell.getSheet().getWorkbook() == mcell.getSheet()
                            .getWorkbook()) {
                        mcell.setCellStyle(cell.getCellStyle());
                    } else {
                        int stHashCode = cell.getCellStyle().hashCode();
                        XSSFCellStyle newCellStyle = styleMap.get(stHashCode);
                        if (newCellStyle == null) {
                            newCellStyle = mcell.getSheet().getWorkbook()
                                    .createCellStyle();
                            newCellStyle.cloneStyleFrom(cell.getCellStyle());
                            styleMap.put(stHashCode, newCellStyle);
                        }
                        mcell.setCellStyle(newCellStyle);
                    }

                     switch (cell.getCellType()) {
                    case FORMULA:
                        mcell.setCellFormula(cell.getCellFormula());
                        break;
                    case  NUMERIC:
                        mcell.setCellValue(cell.getNumericCellValue());
                        break;
                    case STRING:
                        mcell.setCellValue(cell.getStringCellValue());
                        break;
                    case BOOLEAN:
                        mcell.setCellValue(cell.getBooleanCellValue());
                        break;
                    case ERROR:
                        mcell.setCellErrorValue(cell.getErrorCellValue());
                        break;
                    default:
                        mcell.setCellValue(cell.getStringCellValue());
                        break;
                    }

                }
            }
        }
        return outWorkbook;
    }
	public static LinkedHashMap<String, Integer> mapHeaders(String[] outColumns,
            XSSFSheet sheet) {
        LinkedHashMap<String, Integer> map = new LinkedHashMap<String, Integer>();
        XSSFRow row = sheet.getRow(0);
        for (String outColumn : outColumns) {
            Integer icol = null;
            for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
                if (row.getCell(i).getStringCellValue().equals(outColumn)) {
                    icol = new Integer(i);
                }
            }
            map.put(outColumn, icol);
        }
        return map;
             }
	
    }


	
	
	

